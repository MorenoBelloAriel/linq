﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TRABAJO1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("•	1. Mostrar en consola todos los números primos.");

            int[] ListNumeros = new int[] { 8, 10, 40, 32, 2, 40, 8, 3, 2, 10, 99, 22, 32, 37, 94, 17, 43, 29, 9, 11 };
            var NumPrimos = from primos in ListNumeros
                            where primos % 2 != 0
                            select primos;
            foreach (var Num in NumPrimos)
            {
                Console.WriteLine("Numeros Primos->: " + Num);
            }
            //suma de los elementos
            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();


            Console.Clear();
            Console.WriteLine("•	 2. Mostrar en consola la suma de todos los elementos.");
            int sumaTodos = 0;
            var SumaElementos = from numeros in ListNumeros
                                where numeros > sumaTodos
                                select numeros;
            foreach (var Num in SumaElementos)
            {

                sumaTodos = sumaTodos + Num;
            }
            Console.WriteLine("Suma de los elementos es de->: " + sumaTodos);
            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();

            // nueva lista 
            Console.Clear();
            Console.WriteLine("•	3. Generar una nueva lista con el cuadrado de los números.");
            var nueva = from forma in ListNumeros
                        where forma > 0
                        select forma;
            var nuevoforma = (from forma in ListNumeros
                              where forma > 0
                              select forma).ToArray();

            Console.Write("Numeros->");

            foreach (var cuadrado in nueva)
            {
                Console.Write(cuadrado + ",");
            }
            Console.Write("\n");
            Console.Write("Al cuadrado-> ");

            foreach (var cuadrado in nuevoforma)
            {
                Console.Write(Math.Pow(cuadrado, 2) + ",");
            }

            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();

            Console.Clear();
            Console.WriteLine("•	4. Generar una nueva lista con los números primos.");
            Console.Write("Lista nueva -> ");

            var NumerosNuevos = (from NuevosPr in ListNumeros
                                 where NuevosPr % 2 != 0
                                 select NuevosPr).ToArray();
            foreach (var NuevosNum in NumerosNuevos)
            {
                Console.Write(NuevosNum + ",");
            }
            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();

            //promedio 
            Console.Clear();
            Console.WriteLine("•	5. Optener el promedio de todos los números mayores a 50");
            Console.WriteLine("Promedio  ");
            IEnumerable<int> NumeroMayor = from NumeroM in ListNumeros
                                           where NumeroM > 50
                                           select NumeroM;
            foreach (var Nmayor in NumeroMayor)
            {
                Console.Write(Nmayor + ",");
            }
            Console.Write("promedio mayor a 50  :" + NumeroMayor.Average());
            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();

            Console.Clear();
            Console.WriteLine("•	6. Contar la cantidad de números pares e impares. ");
            Console.Write("Par y Impar  ");

            var NumeroPar = from contanumer in ListNumeros
                            group contanumer by (contanumer % 2) == 0 into TotalN
                            select TotalN;

            foreach (var NumeroPr in NumeroPar)
            {

                if (NumeroPr.Key)
                {
                    Console.WriteLine("Numeros pares  total : " + NumeroPar.Count());
                }
                if (!NumeroPr.Key)
                {
                    Console.WriteLine("Numeros impares total : " + NumeroPar.Count());

                }
            }
            Console.Write("Par y Impar  ");
            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();

            Console.Clear();
            Console.WriteLine("•	7. Mostrar en consola, el número y la cantidad de veces que este se encuentra en la lista.");


            var repetidos =
                            from Nrepetid in ListNumeros
                            group Nrepetid by Nrepetid into grupoRepe
                            select grupoRepe;

            foreach (var item in repetidos)
            {
                Console.WriteLine("Numero:  " + item.Key + "  --->se repite:   " + item.Count() + "   vez/veces.");
            }

            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();

            Console.Clear();
            Console.WriteLine("•	8. Mostrar en consola los elementos de forma descendente.");
            var ordenarNumeros =
                            from numeroDesc in ListNumeros
                            orderby numeroDesc descending
                            select numeroDesc;

            Console.Write("Numero en forma descendente ->");

            foreach (var item in ordenarNumeros)
            {
                Console.Write(+item + ",");
            }
            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();

            Console.Clear();
            Console.WriteLine("•	9 Mostrar en consola los números únicos.");
            var UnicoNum =
                            from unico in ListNumeros
                            group unico by unico into unicotot
                            where unicotot.Count() == 1
                            select unicotot;

            Console.Write("Numero unicos  ->");

            foreach (var Num in UnicoNum)
            {
                Console.Write(Num.Key + ",");

            }
            Console.WriteLine("\n");
            Console.WriteLine("Pulse  para la siguiente pregunta o punto del deber ");
            Console.ReadKey();

            Console.Clear();
            Console.WriteLine("•	Sumar todos los números únicos de la lista.");
            int SumaTOTALUNI = 0;
            var numerosTotalUnicos =
                from unico in ListNumeros
                group unico by unico into sumaGroup
                where sumaGroup.Count() == 1
                select sumaGroup;

            foreach (var sumaa in numerosTotalUnicos)
            {
                SumaTOTALUNI = SumaTOTALUNI + sumaa.Key;
            }

            Console.WriteLine("La suma de nùmeros ùnicos es: " + SumaTOTALUNI);

            Console.WriteLine("\n");
            Console.WriteLine("Fin del deber :)  ");
            Console.ReadKey();
        }
    }
}
